extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _rng : RandomNumberGenerator

# Called when the node enters the scene tree for the first time.
func _ready():
	_rng = RandomNumberGenerator.new()
	_rng.randomize()

func get_rng() -> RandomNumberGenerator:
	return _rng

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
