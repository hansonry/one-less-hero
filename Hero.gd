extends Spatial

class_name Hero

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _lane_change_timeout : float = 0.35

var lane : int
var _player_lane : int
var _lane_change_timer : float
var _attack_timer : float

var _next_animation : String

var _sprite : AnimatedSprite3D

var _wants_to_throw : bool

var _puppet_mode

signal attack()

# Called when the node enters the scene tree for the first time.
func _ready():
	_sprite = get_node("AnimatedSprite3D")




func reset():
	lane = 2
	_lane_change_timer = _lane_change_timeout
	show()
	_attack_timer = 4
	_sprite.animation = "Run"
	_next_animation = "Run"
	_wants_to_throw = false


func set_puppet_mode(value: bool):
	_puppet_mode = value

func set_player_lane(lane):
	_player_lane = lane

func _change_lane_tward_player():
	if lane > _player_lane:
		lane -= 1
	elif lane < _player_lane:
		lane += 1

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not _puppet_mode:
		if _player_lane != lane:
			if _lane_change_timer < delta:
				_lane_change_timer = _lane_change_timeout
				_change_lane_tward_player()
			else:
				_lane_change_timer -= delta
		if _attack_timer <= delta:
			_wants_to_throw = true
			_next_animation = "Equip"
			
			_attack_timer = 4
		else:
			_attack_timer -= delta


func _on_AnimatedSprite3D_animation_finished():
	if _puppet_mode:
		if (_sprite.animation == "IntroShockedTurn" or
			_sprite.animation == "IntroGunSteal"):
			_sprite.animation = "Idle"
	else:
		_sprite.animation = _next_animation
		if _wants_to_throw:
			if _next_animation == "Equip":
				_next_animation = "Throw"
			elif _next_animation == "Throw":
				_next_animation = "Run"
		else:
			_next_animation = "Run"


func _on_AnimatedSprite3D_frame_changed():
	if (_wants_to_throw and 
		_sprite.animation == "Throw" and 
		_sprite.frame == 11):
		_wants_to_throw = false
		emit_signal("attack")
		

func get_sprite() -> AnimatedSprite3D:
	return _sprite
	
