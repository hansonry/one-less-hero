extends Spatial
class_name Outro

var _camera_percent: float = 0


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _cameraSpatial : Spatial
var _main : Spatial
var _runtime_seconds : float
var _lbl_runtime : Label

var _play_outro : bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	_cameraSpatial = get_node("../CamSpatial")
	_main = get_node("..")
	_lbl_runtime = get_node("RunTime")
	_lbl_runtime.hide()

func _pick_random_text(text_array : Array):
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var index = rng.randi_range(0, text_array.size() - 1)
	return text_array[index]

func doit(runtime_seconds : float, best_run_timer : float):
	_play_outro = true
	_runtime_seconds = runtime_seconds
	var text_options = [
		"You're done.",
		"How did it come to this?",
		"Gonna need a new sidekick now.",
		"Caught you.",
		"Some hero you are.",
		"Told you crime doesn't pay.",
		"You're just another criminal now.",
		"This is the end for you.",
		"Work on your cardio.",
		"Just another day.",
		"Call an ambulance... BUT NOT FOR ME."
	]

	var style_text = _pick_random_text(text_options)
	_lbl_runtime.text = "%s\n\nRun Time: %.2f Seconds\nBest Time: %.2f Seconds\nPress the 'R' key to try again" % [style_text, _runtime_seconds, best_run_timer]
	_lbl_runtime.show()

func reset():
	_play_outro = false
	_camera_percent = false
	_lbl_runtime.hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if _play_outro:
		_camera_percent += delta
		if _camera_percent > 1:
			_camera_percent = 1
			
		var sigmoid = _main.sigmoid(_camera_percent)
		_cameraSpatial.transform = _cameraSpatial.transform.interpolate_with( 
										_main.get_camera_location_node("Outro").transform,
										sigmoid)

