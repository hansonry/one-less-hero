extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _hero : Hero
var lane
var _vel_x
var _no_effect_timer : float 
var _blink_timer : float
var _hero_dropped : bool

var _sprite : AnimatedSprite3D

# Called when the node enters the scene tree for the first time.
func _ready():
	_vel_x = 2
	_no_effect_timer = 0
	_blink_timer = 0
	_hero_dropped = false
	_sprite = get_node("AnimatedSprite3D")

func throw(hero :Hero):
	_hero = hero
	lane = hero.lane
	transform = hero.transform
	transform.origin.y += 0.14
	transform.origin.x += 0.14

func kick():
	_vel_x = -_vel_x
	_no_effect_timer = 1

func can_hurt():
	return _no_effect_timer == 0
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if  _hero_dropped or not _hero.visible:
		if transform.origin.x < -3:
			get_parent().remove_child(self)
	else: 
		if transform.origin.x < _hero.transform.origin.x:
			if _hero.lane == lane:
				get_parent().remove_child(self)
			else:
				_hero_dropped = true
			
	_vel_x -= delta
	transform.origin.x += _vel_x * delta
	if _no_effect_timer <= delta:
		_no_effect_timer = 0
	else: 
		_no_effect_timer -= delta
	
	if !can_hurt():
		if _blink_timer <= delta:
			_blink_timer = 0.05
			_sprite.visible = !_sprite.visible
		else:
			_blink_timer -= delta
