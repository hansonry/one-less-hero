extends Spatial
class_name Intro

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

enum State {Entry, Running, Event, Exit}

var _audio_stream_player : AudioStreamPlayer
var _intro_audio_stream_player : AudioStreamPlayer
var _cityfx_music_stream_loop
var _cameraSpatial : Spatial
var _main : Spatial
var _sound_engine :GodotAudioIsDumb 

# Sound And music
var _music_intro_a
var _music_intro_b
var _sfx_jump
var _sfx_snatch
var _sfx_gun_break
var _sfx_gun_spin
var _sfx_text_char_hero
var _sfx_text_char_player
var _sfx_birb_takeoff

var _lbl_sidekick_text : Label
var _lbl_hero_text     : Label
var _game : Game
var _hero : Hero
var _player : Player
var _hero_sprite      : AnimatedSprite3D
var _player_sprite    : AnimatedSprite3D
var _dead_guy_sprite  : AnimatedSprite3D
var _gun_parts_sprite : AnimatedSprite3D

var _text_speed : float = 0.08

var _cam_pan_percent : float
var _text_timer : float
var _skip_final_delay: bool
var _state_function : FuncRef

var _player_vel : Vector2
var _jump_in_progress : bool

var _birbs : Spatial

# Called when the node enters the scene tree for the first time.
func _ready():
	_sound_engine = get_node("../GodotAudioIsDumb")
	_audio_stream_player = get_node("../AudioStreamPlayer")
	_intro_audio_stream_player = get_node("IntroAudioStreamPlayer")
	_cameraSpatial = get_node("../CamSpatial")
	_lbl_sidekick_text = get_node("SidekickText")
	_lbl_hero_text     = get_node("HeroText")
	_dead_guy_sprite   = get_node("DeadGuy")
	_gun_parts_sprite  = get_node("GunParts")
	_main = get_node("..")
	_game = get_node("../Game")
	_hero = get_node("../Game/Hero")
	_player = get_node("../Game/Player")
	_hero_sprite   = _hero.get_sprite()
	_player_sprite = _player.get_sprite()
	_hero_sprite.connect("frame_changed", self, "_on_hero_AnimatedSprite3D_frame_changed")
	
	# Music
	_cityfx_music_stream_loop  = load("assets/OLH_CityAmbience.ogg")
	_audio_stream_player.stream = _cityfx_music_stream_loop
	_audio_stream_player.playing = true
	_music_intro_a = load("assets/OLH_Intro_Start.ogg")
	_music_intro_a.loop = false
	_music_intro_b = load("assets/OLH_Intro_Loop.ogg")
	
	# Sound
	_sfx_jump             = _sound_engine.load_sound("assets/OneLessHero_Jump.wav")
	_sfx_snatch           = _sound_engine.load_sound("assets/OLH_Intro_Snatch_Gun.wav")
	_sfx_gun_break        = _sound_engine.load_sound("assets/OLH_Intro_Snap_Gun.wav")
	_sfx_gun_spin         = _sound_engine.load_sound("assets/OLH_Intro_Gun_Spin.wav")
	_sfx_text_char_hero   = _sound_engine.load_sound("assets/OneLessHero_Text_Animation.wav")
	_sfx_text_char_player = _sound_engine.load_sound("assets/OLH_text_sidekick.wav")
	_sfx_birb_takeoff     = _sound_engine.load_sound("assets/OLH_bird.wav")
	
	# State Machine
	_state_function = FuncRef.new()
	_state_function.set_instance(self)
	
	# Birb
	_birbs = get_node("Birbs")
	
	# Setup
	_set_state("_state_birbs")
	_lbl_hero_text.hide()
	_lbl_sidekick_text.hide()
	_player.set_puppet_mode(true)
	_hero.set_puppet_mode(true)
	_player_vel = Vector2()
	_dead_guy_sprite.frame = 0
	_dead_guy_sprite.playing = false
	_game.clean_obstacles(2, -0.5, 1.5)
	_game.clean_obstacles(3, -0.5, 1.5)
	_gun_parts_sprite.frame = 0
	_gun_parts_sprite.hide()
	_gun_parts_sprite.playing = false

func _set_state(function_name: String):
	if _state_function.is_valid():
		_state_function.call_func(State.Exit, 0,  null)
	_state_function.function = function_name
	if _state_function.is_valid():
		_state_function.call_func(State.Entry, 0, null)
	

func _is_key_pressed(event):
	return (event.is_action_pressed("ctrl_jump") or
			event.is_action_pressed("ctrl_up") or
			event.is_action_pressed("ctrl_down") or
			event.is_action_pressed("ctrl_left") or
			event.is_action_pressed("ctrl_right"))


func _input(event):
	if _state_function.is_valid():
		_state_function.call_func(State.Event, 0, event)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if _state_function.is_valid():
		_state_function.call_func(State.Running, delta, null)
		if _player_vel.y > 0 or _player.transform.origin.y > 0:
			_player_vel.y += delta * -5
			_player.transform.origin.x += _player_vel.x * delta
			_player.transform.origin.y += _player_vel.y * delta
			if _player_vel.y < -0.6:
				_player_sprite.frame = 0
			elif _player_vel.y > 0.6:
				_player_sprite.frame = 2
			else:
				_player_sprite.frame = 1
		elif _jump_in_progress:
			_jump_in_progress = false
			_player_sprite.animation = "Idle"
			_player_sprite.playing = true


func _player_jump_back():
	_player_sprite.animation = "IntroJumpBack"
	_player_sprite.playing = false
	_player_vel = Vector2(1, 1)
	_jump_in_progress = true
	_sound_engine.play_sound(_sfx_jump)

func _start_game():
	_player_sprite.animation = "IntroTurn"
	_player_sprite.frame = 0
	_player_sprite.playing = true
	_hero_sprite.animation = "Run"
	_hero_sprite.frame = 2
	_hero_sprite.playing = true
	_set_state("")
	_intro_audio_stream_player.playing = false
	_intro_audio_stream_player.stream = null
	_player.set_puppet_mode(false)
	_hero.set_puppet_mode(false)
	_main.game_start()
	_lbl_hero_text.hide()
	_lbl_sidekick_text.hide()

func _fly_birbs():
	_sound_engine.play_sound(_sfx_birb_takeoff)
	for birb in _birbs.get_children():
		birb.fly_away()

func _state_birbs(state, delta, event):
	if state == State.Event:
		if _is_key_pressed(event):
			_fly_birbs()
			_intro_audio_stream_player.stream = _music_intro_a
			_intro_audio_stream_player.playing = true
			_set_state("_state_pan_down")
	elif state == State.Running:
		pass


var _timer : float = 2
func _state_pan_down(state, delta, event):
	if state == State.Entry:
		_cam_pan_percent = 0
		_player_sprite.animation = "IntroLaughing"
		_player_sprite.playing = true
		_hero_sprite.animation = "IntroShockedTurn"
		_hero_sprite.frame = 0
		_hero_sprite.playing = false

	elif state == State.Event:
		if _is_key_pressed(event):
			_set_state("_state_dialog_sidekick_1")
			#_start_game()
	elif state == State.Running:
		if _timer <= delta:
			_timer = 0
			_cam_pan_percent += delta * 0.1
			var done = false
			if _cam_pan_percent > .25:
				_cam_pan_percent = 1
				done = true
				
			var sigmoid = _main.sigmoid(_cam_pan_percent)
			#print(_cam_pan_percent, "   ", sigmoid)
			_cameraSpatial.transform = _cameraSpatial.transform.interpolate_with( 
											_main.get_camera_location_node("Game").transform,
											sigmoid)
			if done:
				_set_state("_state_dialog_sidekick_1")

		else:
			_timer -= delta
		
	elif state == State.Exit:
		_cameraSpatial.transform = _main.get_camera_location_node("Game").transform
										

func _text_animation_start(label :Label, text: String):
	label.text = text
	label.visible_characters = 0
	label.show()
	_text_timer = 0
	_skip_final_delay = false

func _text_animation_update(label : Label, final_delay: float, delta):
	_text_timer += delta
	if label.visible_characters > -1:
		if _text_timer > _text_speed:
			_text_timer -= _text_speed
			
			label.visible_characters += 1
			if label == _lbl_hero_text:
				_sound_engine.play_sound(_sfx_text_char_hero, -7)
			else:
				_sound_engine.play_sound(_sfx_text_char_player, -7)	
			if label.visible_characters == label.text.length():
				label.visible_characters = -1
	if _text_timer > final_delay or _skip_final_delay:
		_text_timer = final_delay
		return true
	return false

func _state_dialog_sidekick_1(state, delta, event):
	if state == State.Entry:
		_text_animation_start(_lbl_sidekick_text, "That's the last time we deal with you.")
		_hero_sprite.playing = true
	elif state == State.Running:
		if _text_animation_update(_lbl_sidekick_text, 0.5, delta):
			_set_state("_state_dialog_sidekick_2")
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_sidekick_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_sidekick_2(state, delta, event):
	if state == State.Entry:
		_player_sprite.animation = "IntroGunFlaunt"
		_player_sprite.frame = 0
		_dead_guy_sprite.frame = 1
		_sound_engine.play_sound(_sfx_gun_spin)
		_text_animation_start(_lbl_sidekick_text, "Scum.")
	elif state == State.Running:
		if _text_animation_update(_lbl_sidekick_text, 0.5, delta):
			_set_state("_state_dialog_hero_3")
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_sidekick_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_hero_3(state, delta, event):
	if state == State.Entry:
		_text_animation_start(_lbl_hero_text, "FOR THE LAST TIME, RED!")
	elif state == State.Running:
		if _text_animation_update(_lbl_hero_text, 0.5, delta):
			_set_state("_state_dialog_hero_4")
			pass
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_hero_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_hero_4(state, delta, event):
	if state == State.Entry:
		_lbl_sidekick_text.hide()
		_text_animation_start(_lbl_hero_text, "WE.")
	elif state == State.Running:
		if _text_animation_update(_lbl_hero_text, 0.5, delta):
			_set_state("_state_dialog_hero_5")
			pass
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_hero_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_hero_5(state, delta, event):
	if state == State.Entry:
		_lbl_sidekick_text.hide()
		_text_animation_start(_lbl_hero_text, "DON'T.")
	elif state == State.Running:
		if _text_animation_update(_lbl_hero_text, 0.5, delta):
			_set_state("_state_dialog_hero_6")
			pass
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_hero_text.visible_characters = -1
			_skip_final_delay = true
			
func _state_dialog_hero_6(state, delta, event):
	if state == State.Entry:
		_hero_sprite.animation = "IntroGunSteal"
		_lbl_sidekick_text.hide()
		_text_animation_start(_lbl_hero_text, "KILL.")
	elif state == State.Running:
		if _text_animation_update(_lbl_hero_text, 0.5, delta):
			_set_state("_state_dialog_sidekick_7")
			pass
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_hero_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_sidekick_7(state, delta, event):
	if state == State.Entry:
		_player_sprite.animation = "IntroAngry"
		_text_animation_start(_lbl_sidekick_text, "Hey! There's one less villain in the world now, right?")
	elif state == State.Running:
		if _text_animation_update(_lbl_sidekick_text, 0.5, delta):
			_set_state("_state_dialog_hero_8")
			pass
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_sidekick_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_hero_8(state, delta, event):
	if state == State.Entry:
		_text_animation_start(_lbl_hero_text, "No, there isn't.")
	elif state == State.Running:
		if _text_animation_update(_lbl_hero_text, 0.5, delta):
			_set_state("_state_dialog_hero_9")
			pass
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_hero_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_hero_9(state, delta, event):
	if state == State.Entry:
		_lbl_sidekick_text.hide()
		_text_animation_start(_lbl_hero_text, "But there is one less hero.")
	elif state == State.Running:
		if _text_animation_update(_lbl_hero_text, 0.5, delta):
			_set_state("_state_dialog_sidekick_10")
			pass
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_hero_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_sidekick_10(state, delta, event):
	if state == State.Entry:
		_player_jump_back()
		_text_animation_start(_lbl_sidekick_text, "I'm the real hero here!")
	elif state == State.Running:
		if _text_animation_update(_lbl_sidekick_text, 0.5, delta):
			_set_state("_state_dialog_hero_11")
			pass
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_sidekick_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_hero_11(state, delta, event):
	if state == State.Entry:
		_text_animation_start(_lbl_hero_text, "Red, your're no hero.")
	elif state == State.Running:
		if _text_animation_update(_lbl_hero_text, 0.5, delta):
			_set_state("_state_dialog_hero_12")
			pass
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_hero_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_hero_12(state, delta, event):
	if state == State.Entry:
		_lbl_sidekick_text.hide()
		_text_animation_start(_lbl_hero_text, "Heroes don't kill.")
	elif state == State.Running:
		if _text_animation_update(_lbl_hero_text, 0.5, delta):
			_set_state("_state_dialog_hero_13")
			pass
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_hero_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_hero_13(state, delta, event):
	if state == State.Entry:
		_text_animation_start(_lbl_hero_text, "Enough, you're coming with me.")
	elif state == State.Running:
		if _text_animation_update(_lbl_hero_text, 0.5, delta):
			_set_state("_state_dialog_sidekick_14")
			pass
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_hero_text.visible_characters = -1
			_skip_final_delay = true

func _state_dialog_sidekick_14(state, delta, event):
	if state == State.Entry:
		_text_animation_start(_lbl_sidekick_text, "NO!")
	elif state == State.Running:
		if _text_animation_update(_lbl_sidekick_text, 0.5, delta):
			_start_game()
	elif state == State.Event:
		if _is_key_pressed(event):
			_lbl_sidekick_text.visible_characters = -1
			_skip_final_delay = true
			_start_game()

func _on_IntroAudioStreamPlayer_finished():
	if _intro_audio_stream_player.stream == _music_intro_a:
		_intro_audio_stream_player.stream = _music_intro_b
		_intro_audio_stream_player.playing = true

func _on_hero_AnimatedSprite3D_frame_changed():
	if _hero_sprite.animation == "IntroGunSteal":
		if _hero_sprite.frame == 2:
			_sound_engine.play_sound(_sfx_snatch)
		elif _hero_sprite.frame == 9:
			_sound_engine.play_sound(_sfx_gun_break, -8)
		elif _hero_sprite.frame == 13:
			_gun_parts_sprite.show()
			_gun_parts_sprite.playing = true


func _on_GunParts_animation_finished():
	_gun_parts_sprite.frame = 15
	_gun_parts_sprite.playing = false
