extends Spatial

class_name Player

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _sprite :AnimatedSprite3D

var _jump_velocity_frame_threshold : float = 0.2

var _health_max            : float = 3
var health                 : float = _health_max
var _health_regen_timerout : float = 5
var _health_regen_timer    : float 
var _health_regen_rate     : float = 1
var _pushback_on_hit       : float = 0.08
var is_alive               : bool = true
var lane                   : int
var pos                    : Vector2
var vel_y                  : float
var _puppet_mode           : bool

var _health_bar_control : TextureProgress

signal death()

# Called when the node enters the scene tree for the first time.
func _ready():
	_sprite = get_node("AnimatedSprite3D")
	_health_bar_control = get_node("Viewport/TextureProgress")
	#respawn()

func set_puppet_mode(value : bool):
	_puppet_mode = value

func kill():
	if is_alive:
		#rotate_z(deg2rad(90))
		_sprite.animation = "Run"
		#_sprite.playing = false
		#_sprite.frame = 2
		is_alive = false
		emit_signal("death")
		_health_bar_control.visible = false


func _update_heath_bar():
	_health_bar_control.value = health / _health_max
	
func take_dammage():
	if is_alive:
		health = health - 1
		_health_regen_timer = _health_regen_timerout
		_update_heath_bar()
		pos.x -= _pushback_on_hit
		
		if health <= 0:
			#kill()
			pass


func respawn():
	show()
	health = _health_max
	_update_heath_bar()
	_health_regen_timer = 0
	is_alive = true
	lane = 2
	_health_bar_control.visible = false
	_sprite.animation = "Run"
	_sprite.frame = 0
	_sprite.playing = true
	

func _health_regeneration(delta):
	var regen_health :bool
	if _health_regen_timer <= delta:
		_health_regen_timer = 0
		regen_health = true
	else:
		_health_regen_timer = _health_regen_timer - delta
		regen_health = false
	
	if regen_health:
		health = health + _health_regen_rate * delta
		if health > _health_max:
			health = _health_max
		_update_heath_bar()
	
func _animation(delta):
	if pos.y <= 0:
		_sprite.animation = "Run"
	else:
		_sprite.animation = "Jump"
		if vel_y > _jump_velocity_frame_threshold:
			_sprite.frame = 0
		elif vel_y < -_jump_velocity_frame_threshold:
			_sprite.frame = 2
		else:
			_sprite.frame = 1 

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not _puppet_mode:
		_health_regeneration(delta)
		if is_alive:
			_animation(delta)

func get_sprite() -> AnimatedSprite3D:
	return _sprite
	


func _on_AnimatedSprite3D_animation_finished():
	if _sprite.animation == "IntroGunFlaunt":
		# This is some intro stuff. This encapolation is horrible
		_sprite.animation = "Idle"
		_sprite.frame = 0
	elif _sprite.animation == "IntroTurn":
		_sprite.animation = "Run"
		_sprite.frame = 2
		_sprite.playing = true
		
