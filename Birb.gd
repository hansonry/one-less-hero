extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _vel :Vector2
var _is_flying: bool
var _sprite : AnimatedSprite3D
# Called when the node enters the scene tree for the first time.
func _ready():
	_is_flying = false
	_sprite = get_node("AnimatedSprite3D")
	pass # Replace with function body.


func fly_away():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var speed = rng.randf_range(2, 2)
	var angle = rng.randf_range(deg2rad(30), deg2rad(60))
	_vel.x = cos(angle) * speed
	_vel.y = sin(angle) * speed
	_is_flying = true
	_sprite.animation = "Fly"
	_sprite.frame = 1
	_sprite.playing = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if _is_flying:
		transform.origin.x += _vel.x * delta
		transform.origin.y += _vel.y * delta
		
		if transform.origin.y > 6:
			get_parent().remove_child(self)
	pass
