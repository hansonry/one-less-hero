extends Spatial

class_name Game

# Constants
var _travel_speed = 0
var _travel_speed_min = 2.0
var _travel_speed_max = 4.0
var _travel_speed_acceleration = 0.05
var _player_hero_spacing = 0.2
var _tackle_distance = 0.10
var _player_speed = 0.85
var _hero_run_speed = 1
var _camera_min = -20
var _camera_max = 5
var _player_x_max = 1.8
var _player_x_min = -1.8
var _number_of_lanes = 5
var _acceleration_gravity = -5
var _acceleration_gravity_jump = -3
var _jump_count_max = 1
# Test texture size is 4, 10 / 70 pixels are buffer
var _road_width = 3.429 # 4 * 60 / 70
var _jump_speed_vert = 1.5
var _tackle_drag_speed = 1

var rng = RandomNumberGenerator.new()

var _roads     : Spatial
var _buildings : Spatial
var _obstacles : Spatial
var _attacks   : Spatial

var _cityBuildingScene
var _box_obsticle_scene
var _dumpster_obsticle_scene
var _player : Player
var _hero : Hero
var _tackle : Spatial
var _player_jump_count

var _hero_attack_boomer

# Music
var _music_volume_db = -12
var _audio_stream_player
var _background_music_stream_intro
var _background_music_stream_loop
var _mute_flag = false
var _filter_high_pass : AudioEffectHighPassFilter
var _filter_low_pass  : AudioEffectLowPassFilter
var _filter_low_pass_normal       : float = 20000
var _filter_high_pass_normal      : float = 20
var _filter_low_pass_squish       : float = 4000
var _filter_high_pass_squish      : float = 1000
var _filter_to_squish_rate        : float = 1
var _filter_from_squish_rate      : float = 2
var _filter_squish_percent        : float = 0
var _filter_squish_percent_target : float = 0 

# Sound Effects
var _sound_engine              : GodotAudioIsDumb
var _volume_db_jump            : float = 0 
var _audio_stream_jump         : AudioStream
var _volume_db_thud            : float = 0 
var _audio_stream_thud         : AudioStream
var _volume_db_switch          : float = -18
var _audio_stream_switch       : AudioStream
var _volume_db_tackle          : float = 0
var _audio_stream_tackle       : AudioStream
var _volume_db_boomer_throw    : float = -4.5
var _audio_stream_boomer_throw : AudioStream
var _volume_db_hero_hit        : float = 0
var _audio_stream_hero_hit     : AudioStream

# Data
var _obstacle_data : Dictionary
var _road_data     : Dictionary
var _building_data : Array
var run_timer      : float
var best_run_timer : float

# Intro
var _pause        : bool
var _dead_guy     : Spatial
var _dead_guy_box : Spatial
var _gun_parts    : Spatial
var _billboard    : Spatial

signal hard_reset()

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	_roads     = get_node("Roads")
	_buildings = get_node("Buildings")
	_obstacles = get_node("Obstacles")
	_attacks   = get_node("Attacks")
	
	_player    = get_node("Player")
	_hero      = get_node("Hero")
	_tackle    = get_node("Tackle")
	#_cityRoadScene      = load("roadScenes/CityRoad.tscn")
	#_cityRoadScene      = load("roadScenes/MeasureRoad.tscn")
	_cityBuildingScene  = load("buildingScenes/Building.tscn")
	
	_box_obsticle_scene          = load("obstacleScenes/Box.tscn")
	_dumpster_obsticle_scene     = load("obstacleScenes/Dumpster.tscn")
	var car_obsticle_scene       = load("obstacleScenes/Car.tscn")
	var cardboard_obsticle_scene = load("obstacleScenes/Cardboard.tscn")
	var van_obsticle_scene       = load("obstacleScenes/Van.tscn")
	
	_hero_attack_boomer = load("attackScenes/Boomer.tscn")
	
	
	_obstacle_data = {
		"box"       : { "width": .16, "height": .16, "yOffset": 0.08, "scene": _box_obsticle_scene },
		"dumpster"  : { "width": .41, "height": .20, "yOffset": 0.11, "scene": _dumpster_obsticle_scene },
		"car"       : { "width": .41, "height": .20, "yOffset": 0.11, "scene": car_obsticle_scene },
		"cardboard" : { "width": .19, "height": .09, "yOffset": 0.05, "scene": cardboard_obsticle_scene },
		"van"       : { "width": .60, "height": .25, "yOffset": 0.15, "scene": van_obsticle_scene },
	}
	
	_road_data = {
		"rainbow" : { "width": 22,     "scene": load("roadScenes/RainbowRoad.tscn") },
		"city"    : { "width": 29.257, "scene": load("roadScenes/CityRoad.tscn") },
	}
	
	_building_data = [
		#{ "width": 1.84, "scene": load("buildingScenes/Building.tscn") },
		{ "width": 0.32, "scene": load("buildingScenes/Road.tscn") },
		{ "width": 0.32, "scene": load("buildingScenes/Road.tscn") },
		{ "width": 0.32, "scene": load("buildingScenes/Road.tscn") },
		{ "width": 0.32, "scene": load("buildingScenes/Road.tscn") },
		{ "width": 0.32, "scene": load("buildingScenes/Road.tscn") },
		{ "width": 0.32, "scene": load("buildingScenes/Road.tscn") },
		{ "width": 0.32, "scene": load("buildingScenes/Road.tscn") },
		{ "width": 0.32, "scene": load("buildingScenes/Road.tscn") },
		{ "width": 0.32, "scene": load("buildingScenes/Road.tscn") },
		{ "width": 1.32, "scene": load("buildingScenes/newBuildings/Building1-1.tscn") },
		{ "width": 1.32, "scene": load("buildingScenes/newBuildings/Building1-2.tscn") },
		{ "width": 1.32, "scene": load("buildingScenes/newBuildings/Building1-3.tscn") },
		{ "width": 1.32, "scene": load("buildingScenes/newBuildings/Building1-4.tscn") },
		{ "width": 1.98, "scene": load("buildingScenes/newBuildings/Building2-1.tscn") },
		{ "width": 1.98, "scene": load("buildingScenes/newBuildings/Building2-2.tscn") },
		{ "width": 1.98, "scene": load("buildingScenes/newBuildings/Building2-3.tscn") },
		{ "width": 1.98, "scene": load("buildingScenes/newBuildings/Building2-4.tscn") },
		{ "width": 1.32, "scene": load("buildingScenes/newBuildings/Building3-1.tscn") },
		{ "width": 1.32, "scene": load("buildingScenes/newBuildings/Building3-2.tscn") },
		{ "width": 1.32, "scene": load("buildingScenes/newBuildings/Building3-3.tscn") },
		{ "width": 1.32, "scene": load("buildingScenes/newBuildings/Building3-4.tscn") },
		{ "width": 1.98, "scene": load("buildingScenes/newBuildings/Building4-1.tscn") },
		{ "width": 1.98, "scene": load("buildingScenes/newBuildings/Building4-2.tscn") },
		{ "width": 1.98, "scene": load("buildingScenes/newBuildings/Building4-3.tscn") },
		{ "width": 1.98, "scene": load("buildingScenes/newBuildings/Building4-4.tscn") },
	]
	
	# Sound
	_sound_engine = get_node("../GodotAudioIsDumb")
	_audio_stream_jump         = _sound_engine.load_sound("assets/OneLessHero_Jump.wav")
	_audio_stream_thud         = _sound_engine.load_sound("assets/OneLessHero_Obstacle_Thud.wav")
	_audio_stream_switch       = _sound_engine.load_sound("assets/OneLessHero_Lane_Switch.wav")
	_audio_stream_tackle       = _sound_engine.load_sound("assets/OneLessHero_Tackle.wav")
	_audio_stream_boomer_throw = _sound_engine.load_sound("assets/OLH_Boomerang_Whoosh.wav")
	_audio_stream_hero_hit     = _sound_engine.load_sound("assets/OLH_Collide_Hero.wav")
	
	# Music
	_audio_stream_player = get_node("../AudioStreamPlayer")
	_background_music_stream_intro = load("assets/One_Less_Hero_Intro.ogg")
	_background_music_stream_loop  = load("assets/One_Less_Hero_Loop.ogg")
	_background_music_stream_intro.loop = false
	_filter_high_pass = AudioEffectHighPassFilter.new()
	_filter_low_pass  = AudioEffectLowPassFilter.new()
	var bus_index = AudioServer.get_bus_index(_audio_stream_player.bus)
	AudioServer.add_bus_effect(bus_index, _filter_high_pass)
	AudioServer.add_bus_effect(bus_index, _filter_low_pass)
	
	_filter_high_pass.cutoff_hz = 20
	_filter_low_pass.cutoff_hz  = 20000
	
	# Setup
	_manage_roads()
	_manage_buildings()
	_manage_obstacles()
	best_run_timer = 0


func clean_obstacles(lane: int, from_x: float, to_x : float):
	for obstacle in _obstacles.get_children():
		if (obstacle.lane == lane and
			obstacle.transform.origin.x >= from_x and
			obstacle.transform.origin.x <= to_x):
				_obstacles.remove_child(obstacle)


func _clear_all_children(node: Spatial):
	for child in node.get_children():
		node.remove_child(child)

func set_pause(value: bool):
	_pause = value
	if not value:
		_dead_guy     = get_node("../Intro/DeadGuy")
		_dead_guy_box = get_node("../Intro/Box")
		_billboard    = get_node("../Intro/Billboard")
		_gun_parts    = get_node("../Intro/GunParts")

func start_music():
	_audio_stream_player.volume_db = _music_volume_db
	_audio_stream_player.stream  = _background_music_stream_intro
	_audio_stream_player.playing = true


func _update_best_time():
	if best_run_timer < run_timer:
		best_run_timer = run_timer

func _hard_reset():
	_update_best_time()
	reset()
	_clear_all_children(_roads)
	_clear_all_children(_buildings)
	_clear_all_children(_obstacles)
	_manage_roads()
	_manage_buildings()
	_set_player_lane(2)
	_hero.transform.origin.x = _player_x_min
	_player.pos = Vector2(0, 0)
	emit_signal("hard_reset")


func reset():
	_clear_all_children(_attacks)
	
	_travel_speed = _travel_speed_max *  3/4
	_player_jump_count = 0
	_player.respawn()
	_player.pos = Vector2(_player.transform.origin.x, 0)
	
	_hero.reset()
	_hero.set_player_lane(_player.lane)
	_tackle.reset()
	
	_filter_squish_percent_target = 0
	
	Engine.time_scale = 1
	_tackle_drag_speed = 1
	
	run_timer = 0

func get_travel_speed():
	return _travel_speed

	

func _lookup_road_type(road):
	#for road_type in _road_data:
	#	if raod_type["scene"].get_path() == road.get_filename():
	#		pass
	pass

func _manage_roads():
	var minRoadX = _camera_max
	var minRoad = null
	var maxRoadX = _camera_min
	var maxRoad = null
	var max_road_type
	for road in _roads.get_children():
		if road.transform.origin.x < minRoadX:
			minRoadX = road.transform.origin.x
			minRoad = road
		if road.transform.origin.x > maxRoadX:
			maxRoadX = road.transform.origin.x
			maxRoad = road
	if minRoadX < _camera_min:
		# Road is out of camera range, remove it
		_roads.remove_child(minRoad)
	while maxRoadX < _camera_max:
		var newRoad = _road_data["city"]["scene"].instance()
		var roadLength = 29.257
		maxRoadX = maxRoadX + roadLength
		newRoad.transform.origin.x = maxRoadX 
		_roads.add_child(newRoad)

func _manage_buildings():
	var minBuildingX = _camera_max
	var minBuilding = null
	var maxBuildingX = _camera_min
	var maxBuilding = null
	for building in _buildings.get_children():
		if building.transform.origin.x < minBuildingX:
			minBuildingX = building.transform.origin.x
			minBuilding = building
		if building.transform.origin.x > maxBuildingX:
			maxBuildingX = building.transform.origin.x
			maxBuilding = building
	if minBuildingX < _camera_min:
		# Road is out of camera range, remove it
		_buildings.remove_child(minBuilding)
	while maxBuildingX < _camera_max:
		var building_index = rng.randi_range(0, _building_data.size() - 1)
		var building_data = _building_data[building_index]
		var newBuilding = building_data["scene"].instance()
		newBuilding.width = building_data["width"]
		var nextLength
		if maxBuilding == null:
			nextLength = 0
		else:
			nextLength = (newBuilding.width + maxBuilding.width) / 2
		maxBuildingX += nextLength
		newBuilding.transform.origin.x = maxBuildingX
		#newBuilding.transform.origin.y = 0.075 
		#newBuilding.transform.origin.z = -1 
		newBuilding.transform.origin.y = 0 
		newBuilding.transform.origin.z = -2
		maxBuilding = newBuilding 
		_buildings.add_child(newBuilding)

func _manage_obstacles():
	var min_obstacle_x = _camera_max
	var min_obstacle = null
	var max_obstacle_x = _camera_min
	var max_obstacle = null
	for obstacle in _obstacles.get_children():
		if obstacle.active:
			if obstacle.transform.origin.x < min_obstacle_x:
				min_obstacle_x = obstacle.transform.origin.x
				min_obstacle = obstacle
			if obstacle.transform.origin.x > max_obstacle_x:
				max_obstacle_x = obstacle.transform.origin.x
				max_obstacle = obstacle
		else:
			if obstacle.transform.origin.y < -2:
				_obstacles.remove_child(obstacle)
	if min_obstacle_x < _camera_min:
		# Road is out of camera range, remove it
		_obstacles.remove_child(min_obstacle)
	while max_obstacle_x < _camera_max:
		var lane = rng.randi_range(0, 4)
		var obstacle_type
		if lane == 0:
			obstacle_type = _obstacle_data["dumpster"]
		else:
			var rand = rng.randf();
			if rand < 0.01:
				obstacle_type = _obstacle_data["car"]
			elif rand < 0.02:
				obstacle_type = _obstacle_data["van"]
			elif rand < 0.50:
				obstacle_type = _obstacle_data["cardboard"]
			else:
				obstacle_type = _obstacle_data["box"]
				
		
		var new_obstacle = obstacle_type["scene"].instance()
		var next_obstical_at = rng.randf_range(obstacle_type["width"], obstacle_type["width"] + 0.8)
		new_obstacle.lane = lane
		new_obstacle.obstacle_type = obstacle_type
		max_obstacle_x = max_obstacle_x + next_obstical_at
		new_obstacle.transform.origin.x = max_obstacle_x
		new_obstacle.transform.origin.y = obstacle_type["yOffset"]
		new_obstacle.transform.origin.z = _compute_lane_z(new_obstacle.lane)
		new_obstacle.width  = obstacle_type["width"]
		new_obstacle.height = obstacle_type["height"]
		new_obstacle.gravity = _acceleration_gravity
		_obstacles.add_child(new_obstacle)


func _compute_lane_z(lane):
	var lane_size = _road_width / float(_number_of_lanes) 
	return ((lane_size * lane) + ((lane_size - _road_width) / 2.0))

func _set_player_lane(lane):
	_player.lane = lane
	_player.transform.origin.z = _compute_lane_z(_player.lane)
	_hero.set_player_lane(lane)

func _player_moves_up():
	if(_player.lane > 0):
		_set_player_lane(_player.lane - 1)
		_sound_engine.play_sound(_audio_stream_switch, _volume_db_switch)

func _player_moves_down():
	if(_player.lane < 4):
		_set_player_lane(_player.lane + 1)
		_sound_engine.play_sound(_audio_stream_switch, _volume_db_switch)


func _player_jump():
	if _player_jump_count < _jump_count_max:
		_player_jump_count = _player_jump_count + 1 
		_player.vel_y = _jump_speed_vert
		_sound_engine.play_sound(_audio_stream_jump, _volume_db_jump)

func _input(event):
	if not _pause:
		if _player.is_alive:
			if event.is_action_pressed("ctrl_jump"):
				_player_jump()
			elif event.is_action_pressed("ctrl_up"):
				_player_moves_up()
			elif event.is_action_pressed("ctrl_down"):
				_player_moves_down()
		if event.is_action_pressed("ctrl_restart"):
			_hard_reset()
		elif event.is_action_pressed("audio_mute"):
			if _mute_flag:
				_mute_flag = false
				_audio_stream_player.volume_db = _music_volume_db
			else:
				_mute_flag = true
				_audio_stream_player.volume_db = -80.0

func _player_movement(delta, speed_percent):
	var acceleration_gravity = _acceleration_gravity
	if _player.is_alive:
		var delta_x = 0
		if Input.is_action_pressed("ctrl_left"):
			delta_x -= delta *  _player_speed
		elif Input.is_action_pressed("ctrl_right"):
			delta_x += delta *  _player_speed
		if Input.is_action_pressed("ctrl_jump"):
			acceleration_gravity = _acceleration_gravity_jump
		var new_min = lerp(_player_x_min, _player_x_max, speed_percent) + _player_hero_spacing

		
		
		if delta_x > 0:
			_player.pos.x += delta_x
			if _player.pos.x > _player_x_max:
				_player.pos.x = _player_x_max
		elif delta_x < 0:
			var new_x = _player.pos.x + delta_x
			if new_x < new_min:
				new_x = new_min
			if new_x < _player.pos.x:
				_player.pos.x = new_x

	else:
		var motionDelta = delta * _travel_speed
		#_player.pos.x -= motionDelta

	_player.vel_y = _player.vel_y + acceleration_gravity * delta
	_player.pos.y = _player.pos.y + _player.vel_y        * delta
	if _player.pos.y < 0:
		_player.pos.y = 0
		_player.vel_y = 0
		_player_jump_count = 0
	
	_player.transform.origin.x = _player.pos.x
	_player.transform.origin.y = _player.pos.y
		

func _detect_overlap(a_base: Vector3, a_width: float, a_height: float,
					 b_base: Vector3, b_width: float, b_height: float):
	var min_width_dist = (a_width + b_width) / 2
	var x_dist = abs(b_base.x - a_base.x)
	if x_dist < min_width_dist:
		if a_base.y > b_base.y:
			if a_base.y - b_base.y < b_height:
				return true
		elif b_base.y - a_base.y < a_height:
				return true
	return false
	

func _player_hit_something():
	_sound_engine.play_sound(_audio_stream_thud, _volume_db_thud)
	_player.take_dammage()
	if _player.is_alive:
		_travel_speed = _travel_speed - 0.3

func _handle_collision(obstacle: Obstacle):
	obstacle.kick()
	_player_hit_something()


func _detect_collision():
	# Obsticals
	for obstacle in _obstacles.get_children():
		var obOrigin = obstacle.transform.origin
		obOrigin.y -= obstacle.obstacle_type["yOffset"]
		if (obstacle.active and _player.lane == obstacle.lane and 
			_detect_overlap(_player.transform.origin, 0.12, 0.22,
							obOrigin, obstacle.width, obstacle.height)):
				_handle_collision(obstacle)
	# Hero Hits Obstacles
	for obstacle in _obstacles.get_children():
		var obOrigin = obstacle.transform.origin
		obOrigin.y -= obstacle.obstacle_type["yOffset"]
		if (obstacle.active and _hero.lane == obstacle.lane and 
			_detect_overlap(_hero.transform.origin, 0.24, 0.22,
							obOrigin, obstacle.width, obstacle.height)):
				obstacle.kick()
				_sound_engine.play_sound(_audio_stream_hero_hit, _volume_db_hero_hit)
	# Attacks
	if _player.is_alive:
		for attack in _attacks.get_children():
			var attack_origin = attack.transform.origin
			attack_origin.y -= 0.035
			if (attack.can_hurt() and _player.lane == attack.lane and
				_detect_overlap(_player.transform.origin, 0.12, 0.22,
								attack_origin, 0.14, 0.07)):
				attack.kick()
				_player_hit_something()

func _slew(current : float, target : float, delta : float):
	if current == target:
		return target
	elif current > target:
		if current - target <= delta:
			return target
		else:
			return current - delta
	else:
		if target - current <= delta:
			return target
		else:
			return current + delta

func _squish_audio_effect(delta):
	var rate = _filter_to_squish_rate
	if _filter_squish_percent > _filter_squish_percent_target:
		rate = _filter_from_squish_rate
	
	_filter_squish_percent = _slew(_filter_squish_percent, 
								   _filter_squish_percent_target, 
								   rate * delta)
	var low_pass_value = lerp(_filter_low_pass_normal, _filter_low_pass_squish, _filter_squish_percent)
	var high_pass_value = lerp(_filter_high_pass_normal, _filter_high_pass_squish, _filter_squish_percent)
	_filter_high_pass.cutoff_hz = high_pass_value
	_filter_low_pass.cutoff_hz  = low_pass_value


func _hero_movement(delta, speed_percent):
	if _player.is_alive:
		var hero_target = lerp(_player_x_min, _player_x_max, speed_percent)
		_hero.transform.origin.x = _slew(_hero.transform.origin.x, hero_target, _hero_run_speed * delta)
	elif not _tackle.is_in_process():
		_hero.transform.origin.x = _player.transform.origin.x - _tackle_distance
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not _pause:
		var motionDelta = delta * _travel_speed
		var speed_percent = ((_travel_speed     - _travel_speed_min) /
							 (_travel_speed_max - _travel_speed_min))
		# We want the hero to approch slowly at first. Then when death is near
		# Approch quickly. That way the player has on average more space
		speed_percent = 1 - speed_percent
		speed_percent *= speed_percent
		
		for road in _roads.get_children():
			road.transform.origin.x -= motionDelta
		for building in _buildings.get_children():
			building.transform.origin.x -= motionDelta / 1.2
		for obstacle in _obstacles.get_children():
			obstacle.transform.origin.x += obstacle.vel.x * delta - motionDelta
			obstacle.transform.origin.y += obstacle.vel.y * delta
			
		# Intro
		_dead_guy.transform.origin.x -= motionDelta
		_dead_guy_box.transform.origin.x -= motionDelta
		_billboard.transform.origin.x -= motionDelta
		_gun_parts.transform.origin.x -= motionDelta
		
		_manage_roads()
		_manage_buildings()
		_manage_obstacles()

		_player_movement(delta, speed_percent)
		_hero_movement(delta, speed_percent)
		
		_detect_collision()
		
		if _player.pos.x < _hero.transform.origin.x + _tackle_distance:
			if _player.is_alive:
				_update_best_time()
			_player.kill()
			
		
		_hero.transform.origin.z = _compute_lane_z(_hero.lane)
		
		# Ajust acceleration
		if _player.is_alive:
			_travel_speed = _travel_speed + _travel_speed_acceleration * delta
			if _travel_speed > _travel_speed_max:
				_travel_speed = _travel_speed_max
		else:
			_travel_speed -= delta * _tackle_drag_speed
			_tackle_drag_speed += delta * 0.5
			
			if _travel_speed < 0:
				_travel_speed = 0
				_tackle.legs_down()
				
		
		# Audio Effect
		_squish_audio_effect(delta)
		
		if _player.is_alive:
			run_timer += delta

func _on_AudioStreamPlayer_finished():
	if _audio_stream_player.stream == _background_music_stream_intro:
		_audio_stream_player.stream = _background_music_stream_loop
		_audio_stream_player.playing = true


func _on_Player_death():
	_filter_squish_percent_target = 1
	_audio_stream_tackle
	_tackle.doit(_hero)
	_hero.hide()
	Engine.time_scale = 0.25

func _on_Tackle_hide_player():
	_player.hide()
	_sound_engine.play_sound(_audio_stream_tackle, _volume_db_tackle)


func _on_Hero_attack():
	if _player.is_alive and not _pause:
		var boomer = _hero_attack_boomer.instance()
		_sound_engine.play_sound(_audio_stream_boomer_throw, _volume_db_boomer_throw)
		_attacks.add_child(boomer)
		boomer.throw(_hero)

