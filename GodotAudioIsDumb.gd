extends Node

class_name GodotAudioIsDumb
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(String) var bus = "Sfx"
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func play_sound(sound: AudioStream, volume_db : float = 0):
	# Look for an aviable Audio Stream Player
	var audio_stream = null
	for aus in get_children():
		if not aus.playing:
			audio_stream = aus
	if audio_stream == null:
		audio_stream = AudioStreamPlayer.new()
		audio_stream.bus = bus
		add_child(audio_stream)
	audio_stream.stream = sound
	audio_stream.play()
	audio_stream.volume_db = volume_db
	return audio_stream

func load_sound(soundfile):
	var sound = load(soundfile)
	assert(sound != null)
	#sound.loop = false
	return sound
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
