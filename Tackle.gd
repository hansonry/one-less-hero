extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _sprite : AnimatedSprite3D
var _target : Spatial

signal hide_player()
signal end_of_tackle()

# Called when the node enters the scene tree for the first time.
func _ready():
	_sprite = get_node("AnimatedSprite3D")

func reset():
	hide()
	_sprite.frames.set_animation_speed("default", 14)
	_sprite.playing = false
	_sprite.frame = 0
	_target = null


func doit(target):
	_sprite.playing = true
	show()
	_target = target
	transform = target.transform

func is_in_process():
	return _sprite.playing

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if _target != null:
		transform = _target.transform

func legs_down():
	if _sprite.frame == 4 and not _sprite.playing:
		emit_signal("end_of_tackle")
		_sprite.frames.set_animation_speed("default", 4)
		_sprite.playing = true
		

func _on_AnimatedSprite3D_frame_changed():
	if _sprite.frame == 3:
		emit_signal("hide_player")
	elif _sprite.frame == 4:
		_sprite.playing = false
	elif _sprite.frame == 5:
		_sprite.playing = false
