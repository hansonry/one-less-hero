extends Spatial

class_name Obstacle


var lane : int
var width : float
var height : float
var active : bool
var obstacle_type

var gravity : float
var vel : Vector3
var _rot_vel : float


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	active = true
	vel = Vector3()
	_rot_vel = 0

func kick():
	vel.y = 3
	vel.x = 2
	_rot_vel = 12
	active = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate_z(_rot_vel * delta)
	
	vel.y += gravity * delta
	if(active and vel.y < 0):
		vel.y = 0
