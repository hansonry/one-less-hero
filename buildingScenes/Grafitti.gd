extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var global_rng = get_node("/root/GlobalRandomGen")
	
	var sprite :AnimatedSprite3D  = get_node("AnimatedSprite3D")
	var rng = global_rng.get_rng()
	var showChance = rng.randf()
	if showChance < .3:
		var frameIndex = rng.randi_range(0, 14)
		sprite.frame = frameIndex
		var frameRotation = rng.randi_range(-15, 15)
		sprite.rotation_degrees = Vector3(0,0,frameRotation)
	else:
		hide()
		sprite.hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
