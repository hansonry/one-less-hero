extends Spatial

class_name Main

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _game : Game
var _intro: Intro
var _outro: Outro
var _cameraSpatial : Spatial

func _ready():
	_cameraSpatial = get_node("CamSpatial")
	_game          = get_node("Game")
	_intro         = get_node("Intro")
	_outro         = get_node("Outro")
	
	_cameraSpatial.transform = get_camera_location_node("Intro").transform
	_game.set_pause(true)

func get_camera_location_node(name : String) -> Spatial:
	return get_node("CameraLocations/" + name) as Spatial

func game_start():
	_game.reset()
	_game.start_music()
	_game.set_pause(false)

func sigmoid(p :float) -> float:
	var rad = PI * (p -  0.5)
	return (sin(rad) + 1) / 2

func _process(delta):
	pass


func _on_Tackle_end_of_tackle():
	_outro.doit(_game.run_timer, _game.best_run_timer)


func _on_Game_hard_reset():
	_outro.reset()
	_cameraSpatial.transform = get_camera_location_node("Game").transform
	
