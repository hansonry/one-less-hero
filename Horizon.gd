extends Spatial

# constants
var _background_speed = 0.3
var _forground_speed  = .6

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _main 
var _forground1
var _forground2
var _background1
var _background2

# Called when the node enters the scene tree for the first time.
func _ready():
	_main        = get_node("..")
	_forground1  = get_node("Forground1")
	_forground2  = get_node("Forground2")
	_background1 = get_node("Background1")
	_background2 = get_node("Background2")



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var travel_speed = _main.get_travel_speed()
	if travel_speed > 1:
		travel_speed = 1
	
	var motion_forground  = delta * _forground_speed * travel_speed
	var motion_background = delta * _background_speed * travel_speed
	_forground1.transform.origin.x -= motion_forground
	_forground2.transform.origin.x -= motion_forground
	_background1.transform.origin.x -= motion_background
	_background2.transform.origin.x -= motion_background
	
	if _forground1.transform.origin.x < -15.36:
		_forground1.transform.origin.x += 20.48
	if _forground2.transform.origin.x < -15.36:
		_forground2.transform.origin.x += 20.48
	if _background1.transform.origin.x < -15.36:
		_background1.transform.origin.x += 20.48
	if _background2.transform.origin.x < -15.36:
		_background2.transform.origin.x += 20.48

